﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Sol_Login_Page_Asp.net.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">

        table{
            margin:auto;  
            padding:200px;
        }

        .roundedTextBox{
             border:2px;
            border-color:gray;
            border-style:solid;
            border-radius:5px;
            width:400px;
            height:40px;
            padding:2px;
            margin:3px;
        }

        .roundButton {
            border:2px;
            border-color:gray;
            border-style:solid;
            border-radius:5px;
            width:100px;
            height:40px;
            padding:2px;
            margin:10px;
            background-color:cornflowerblue;
            color:white;
            font-size:medium;
            font-weight:700;
        }
       
        
    </style>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script type="text/javascript">


    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="tblClass">
            <tr>
                <td>                    
                    <asp:TextBox ID="txtUserName" runat="server" placeholder="Username" class="roundedTextBox"></asp:TextBox>                   
                </td>
            </tr>

            <tr>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" class="roundedTextBox"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Button ID="btnLogIn" runat="server" Text="Log in" OnClick="btnLogIn_Click" Class="roundButton" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" Text="*Username and password does not match" Visible="false" ForeColor="Red" Font-Size="Smaller"></asp:Label>
                </td>
            </tr>

        </table>
    
    </div>
    </form>
</body>
</html>
