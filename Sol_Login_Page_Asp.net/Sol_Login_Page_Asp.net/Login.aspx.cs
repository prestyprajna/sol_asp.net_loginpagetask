﻿using Sol_Login_Page_Asp.net.Models.Dal;
using Sol_Login_Page_Asp.net.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Login_Page_Asp.net
{
    public partial class Login : System.Web.UI.Page
    {
        //private UserEntity userEntityObj = null;

        protected void Page_Load(object sender, EventArgs e)
        {
           

        }

        protected void btnLogIn_Click(object sender, EventArgs e)
        {           

                UserDal userDalObj = new UserDal();
                var result = userDalObj?.GetUserData(new UserEntity()
                {
                    userLoginEntityObj = new UserLoginEntity()
                    {
                        Username = txtUserName.Text,
                        Password = txtPassword.Text
                    }
                });                

                if (result!=null)
                {
                    Response.Redirect("~/User.aspx");                
                }
                else
                {
                    lblMessage.Visible = true;
                }            
        }        
    }
}