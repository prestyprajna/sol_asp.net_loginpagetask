﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Sol_Login_Page_Asp.net.User" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">

         table{
            margin:auto;     
            font-size:medium;       
        }

         .roundButton {
            border:2px;
            border-color:gray;
            border-style:solid;
            border-radius:5px;
            width:100px;
            height:40px;
            padding:2px;
            margin:10px;
            background-color:sandybrown;
            color:white;
            font-size:medium;
            font-weight:700;            
        }

    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <span>WELCOME</span>
                </td>
                <td>
                    <asp:Label ID="lblFirstName" runat="server" ></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblLastName" runat="server" ></asp:Label>
                </td>

            </tr>

            <tr>
                <td>
                    <asp:Button ID="btnLogout" runat="server" OnClick="btnLogout_Click" Text="Logout" Class="roundButton" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
