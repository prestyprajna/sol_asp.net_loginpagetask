﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Login_Page_Asp.net.Models.Entity
{
    public class UserLoginEntity
    {
        public decimal? UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}