﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Login_Page_Asp.net.Models.Entity
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserLoginEntity userLoginEntityObj { get; set; }
    }
}