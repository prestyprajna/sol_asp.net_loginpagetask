﻿using Sol_Login_Page_Asp.net.Models.Entity;
using Sol_Login_Page_Asp.net.Models.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Login_Page_Asp.net.Models.Dal
{
    public class UserDal
    {
        #region  declaration

        private UserDCDataContext _dc = null;
       

        #endregion

        #region  constructor

        public UserDal()
        {
            _dc = new UserDCDataContext();            
        }

        #endregion

        #region  public methods

        public UserEntity GetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery=
                _dc?.uspGetUserLogin(
                    "GetUserData",
                    userEntityObj?.UserId,
                    userEntityObj?.userLoginEntityObj?.Username,
                    userEntityObj?.userLoginEntityObj?.Password,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leUspLoginResultSetObj) => new UserEntity()
                    {
                        UserId = leUspLoginResultSetObj?.UserId,
                        FirstName = leUspLoginResultSetObj?.FirstName,
                        LastName = leUspLoginResultSetObj?.LastName
                    })
                    ?.FirstOrDefault();

            return getQuery;

            //return (status == 1) ? userEntityObj : (dynamic)status;
        }

        #endregion
    }
}